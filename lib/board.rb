require 'byebug'
class Board
  attr_accessor :grid

  def initialize(grid = empty_board)
    @grid = grid
  end

  def empty_board
    Array.new(3) { Array.new(3) }
  end

  def place_mark(position, mark)
    grid[position.first][position.last] = mark
  end

  def empty?(position)
    grid[position.first][position.last].nil?
  end

  def winner
    horizontal_winner || vertical_winner || diagonal_winner
  end

  def horizontal_winner
    grid.each do |row|
      return :X if row.all? { |item| item == :X }
      return :O if row.all? { |item| item == :Y }
    end
    nil
  end

  def vertical_winner
    col0 = grid.map { |row| row[0] }
    col1 = grid.map { |row| row[1] }
    col2 = grid.map { |row| row[2] }
    [col0, col1, col2].each do |col|
      return :X if col.all? { |item| item == :X }
      return :O if col.all? { |item| item == :O }
    end
    nil
  end

  def left_diagonal_winner
    if [grid[0][0], grid[1][1], grid[2][2]].all? { |mark| mark == :O }
      return :O
    elsif [grid[0][0], grid[1][1], grid[2][2]].all? { |mark| mark == :X }
      return :X
    end
    nil
  end

  def right_diagonal_winner
    if [grid[0][2], grid[1][1], grid[2][0]].all? { |mark| mark == :O }
      return :O
    elsif [grid[0][2], grid[1][1], grid[2][0]].all? { |mark| mark == :X }
      return :X
    end
    nil
  end

  def diagonal_winner
    left_diagonal_winner || right_diagonal_winner
  end

  def over?
    return true if winner
    return true if grid.all? { |row| row.none?(&:nil?) }
    false
  end
end

class HumanPlayer
  attr_reader :name, :board
  attr_accessor :mark
  def initialize(name = 'unnamed player')
    @name = name
  end

  def display(board)
    @board = board
    puts board.grid
  end

  def get_move
    puts 'WHERE?! (put row index and column index, separated by a comma)'
    gets.split(', ').map(&:to_i)
  end

end

class ComputerPlayer
  attr_accessor :mark
  attr_reader :name, :board
  def initialize(comp_name = 'unnamed comp')
    @name = comp_name
  end

  def display(board)
    @board = board
  end

  def get_move
    winning_move || random_move
  end

  def random_move
    all_valid_moves[rand(0..all_valid_moves.length - 1)]
  end

  def winning_move
    all_valid_moves.each do |move|
      test_board = board
      test_board.place_mark(move, mark)
      return move if test_board.winner
    end
    nil
  end

  def all_valid_moves
    all_valid_moves = []
    board.grid.each_with_index do |row, i1|
      row.each_index do |i2|
        all_valid_moves << [i1, i2] if board.grid[i1][i2].nil?
      end
    end
    all_valid_moves
  end
end

class Game
  attr_reader :board, :player_one, :player_two, :current_player
  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    @board = Board.new
    @current_player = player_one
  end

  def switch_players!
    new_player = player_two if current_player == player_one
    new_player = player_one if current_player == player_two
    @current_player = new_player
  end

  def play_turn
    board.place_mark(current_player.get_move, current_player.mark)
    switch_players!
  end
end
